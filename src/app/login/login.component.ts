import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ServiceService } from '../service.service';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private fb: FormBuilder, public http: HttpClient, private authService: ServiceService) {
  };

  formGroup: FormGroup;
  validUser: boolean = true;
  notAuthorized: boolean;

  ngOnInit() {
    this.formGroup = this.fb.group({
      username: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.pattern('((?=.*[A-Z]).{8,})')]],
    });
  }

  login() {
    console.log(this.formGroup.value);

    this.authService
    .login(this.formGroup.get('username')?.value, this.formGroup.get('password')?.value)
    .pipe(take(1))
    .subscribe(
      () => {
        console.log('Success!');
        this.notAuthorized = false;
        this.validUser = true;
      },
      error => {
        console.log(error);
        if(error.status === 401) {
          this.validUser = false;
          this.notAuthorized = true;
        }
      }
    )
  }

  logout() {
    this.authService.logout();
    this.notAuthorized = true;
  }
}
