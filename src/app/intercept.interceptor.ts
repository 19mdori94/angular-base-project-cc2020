import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { ServiceService } from './service.service';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable()
export class InterceptInterceptor implements HttpInterceptor {

  constructor(public authService: ServiceService, private jwtHelper: JwtHelperService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {

    request = request.clone({
      setHeaders: {
        Authorization: `Bearer ${this.jwtHelper.tokenGetter()}`
      }
    });
    return next.handle(request);
  }
}
