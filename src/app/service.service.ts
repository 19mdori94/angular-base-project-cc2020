import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/internal/operators';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  constructor(private http: HttpClient, private jwtHelper: JwtHelperService) { }

  loggedIn: boolean = false;

  login(name: string, pword: string): Observable<any>{
    return this.http.post('http://localhost:8080/login', {
      username: name,
      password: pword,
    }, { responseType: 'text' })
    .pipe(
      map((token) => {
        localStorage.setItem('accessToken', token);
        this.loggedIn = true;
        return token;
      })
    )
  }

  logout() {
    localStorage.removeItem('accessToken');
    this.loggedIn = false;
  }

  public IsTokenValid(): boolean {
    const token = this.jwtHelper.tokenGetter();
    if(token) {
      const decodedToken = this.jwtHelper.decodeToken(token);
      const expiryDate = decodedToken.exp * 1000;
      if(Date.now() > expiryDate) {
        this.logout();
      } else {
        this.loggedIn = true;
      }
    } else {
      this.loggedIn = false;
    }
    return this.loggedIn;
  }
}
